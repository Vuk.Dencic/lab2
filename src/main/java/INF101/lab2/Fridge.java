package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
    private final int maxSize = 20;
    public ArrayList<FridgeItem> items = new ArrayList<>(maxSize);


    @Override
    public int nItemsInFridge() {
       return items.size();
    }

    @Override
    public int totalSize() {
        return maxSize;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if(totalSize() - nItemsInFridge() > 0){
            items.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if(!items.remove(item)){
            throw new NoSuchElementException();
        }
    }

    @Override
    public FridgeItem emptyFridge() {
        items = new ArrayList<>(maxSize);
        return null;
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredItems = new ArrayList<>();
        for(FridgeItem item : items){
            if(item.hasExpired()){
                expiredItems.add(item);
            }
        }
        items.removeAll(expiredItems);
        return expiredItems;
    }
}
